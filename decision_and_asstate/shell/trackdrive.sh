#!/bin/bash
printf "We are running shell for mission: trackdrive!\n"

#根据需要启动不同模块的各个节点的指定launch文件
# roslaunch delaunay delaunay.launch&
roslaunch controller nlp.launch& #启动控制部分节点
# roslaunch boundary_detector  boundary_detector.launch& #启动规划模块节点

#其他模块在不同的Mission下的参数一致，因此设置为开机自启动
printf "Shell for mission: trackdrive has run!\n"