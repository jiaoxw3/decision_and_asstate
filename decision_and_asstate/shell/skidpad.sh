#!/bin/bash
printf "We are running shell for mission: skidpad!\n"

#根据需要启动不同模块的各个节点的指定launch文件
roslaunch controller skidpad.launch& #启动控制部分节点
# roslaunch skidpad_detector skidpad_detector.launch& #启动规划模块节点

#其他模块在不同的Mission下的参数一致，因此设置为开机自启动
printf "Shell for mission: skidpad has run!\n"
