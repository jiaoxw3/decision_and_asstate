#!/bin/bash
printf "We are running shell for mission: inspection!\n"

#根据需要启动不同模块的各个节点的指定launch文件
roslaunch controller inspection.launch& #启动控制部分节点

#其他模块在不同的Mission下的参数一致，因此设置为开机自启动
printf "Shell for mission: inspection has run!\n"