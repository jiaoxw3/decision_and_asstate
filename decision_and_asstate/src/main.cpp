/*
    a brand new decision and autonomous system state machine for SCUTAracing
    @chenshaohao
*/

#include <ros/ros.h>
#include "decision_and_asstate.hpp"

int main(int argc, char **argv) {
  ros::init(argc, argv, "decision_and_asstate");
  ros::NodeHandle nodeHandle("~");
  ns_decision_and_asstate::DecisionAndAsState decisionAndAsState(nodeHandle);
  ros::Rate loop_rate(decisionAndAsState.getNodeRate());
  while (ros::ok()) {
    ros::spinOnce(); 
    decisionAndAsState.run();
    loop_rate.sleep();             
  }
  return 0;
}


