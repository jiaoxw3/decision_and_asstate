/*
    a brand new decision and autonomous system state machine for SCUTAracing
    @chenshaohao
    @lianghaoming
*/
#include <ros/ros.h>
#include "decision_and_asstate.hpp"
#include <sstream>
#include <pthread.h>
#include <stdlib.h>

namespace ns_decision_and_asstate {
//constructor
DecisionAndAsState::DecisionAndAsState(ros::NodeHandle &nodeHandle):
    nodeHandle_(nodeHandle){
    ROS_INFO("Constructing DecisionAndAsState");
    loadParameters();
    subscribeToTopics();
    publishToTopics();
}
// Getters
int DecisionAndAsState::getNodeRate() const { return node_rate_; }
// Methods
void DecisionAndAsState::loadParameters() {
    ROS_INFO("loading DecisionAndAsState parameters");
    if (!nodeHandle_.param<std::string>("lidar_topic_name", lidar_topic_name_, "/rslidar_points")) {
        ROS_WARN_STREAM("Did not load lidar_topic_name. Standard value is: " << lidar_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("slam_state_topic_name", slam_state_topic_name_, "/estimation/slam/state")) {
        ROS_WARN_STREAM("Did not load slam_state_topic_name. Standard value is: " << slam_state_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("camera_topic_name", camera_topic_name_, "/pylon_camera_node/image_raw")) {
        ROS_WARN_STREAM("Did not load camera_topic_name. Standard value is: " << camera_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("ins_topic_name", ins_topic_name_, "/asensing_data")) {
        ROS_WARN_STREAM("Did not load ins_topic_name. Standard value is: " << ins_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("global_center_line_topic_name", global_center_line_topic_name_, "/control/pure_pursuit/center_line")) {
        ROS_WARN_STREAM("Did not load global_center_line_topic_name. Standard value is: " << global_center_line_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("as_state_topic_name", as_state_topic_name_, "/asState")) {
        ROS_WARN_STREAM("Did not load as_state_topic_name. Standard value is: " << as_state_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("control_command_topic_name", control_command_topic_name_, "/control/pure_pursuit/control_command")) {
        ROS_WARN_STREAM("Did not load control_command_topic_name. Standard value is: " << control_command_topic_name_);
    }
    if (!nodeHandle_.param("node_rate", node_rate_, 50)) {
        ROS_WARN_STREAM("Did not load node_rate. Standard value is: " << node_rate_);
    }
    if (!nodeHandle_.param("dis_initial_to_end", dis_initial_to_end_, 2.0)) {
        ROS_WARN_STREAM("Did not load dis_initial_to_end. Standard value is: " << dis_initial_to_end_);
    }
    if (!nodeHandle_.param("dis_new_initial_to_end", dis_new_initial_to_end_, 2.0)) {
        ROS_WARN_STREAM("Did not load dis_new_initial_to_end. Standard value is: " << dis_new_initial_to_end_);
    }
    if (!nodeHandle_.param("dis_target_end", dis_target_end_, 1.0)) {
        ROS_WARN_STREAM("Did not load dis_target_end. Standard value is: " << dis_target_end_);
    }
    if (!nodeHandle_.param("maxLapCount", maxLapCount_, 3)) {
        ROS_WARN_STREAM("Did not load maxLapCount. Standard value is: " << maxLapCount_);
    }
    if (!nodeHandle_.param<std::string>("res_and_ami_topic_name", res_and_ami_topic_name_, "/res_and_ami_data")) {
        ROS_WARN_STREAM("Did not load res_and_ami_topic_name. Standard value is: " << res_and_ami_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("compute_time_topic_name", compute_time_topic_name_, "/compute_time")) {
        ROS_WARN_STREAM("Did not load compute_time_topic_name. Standard value is: " << compute_time_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("control_compute_time_topic_name", control_compute_time_topic_name_, "/control_compute_time")) {
        ROS_WARN_STREAM("Did not load control_compute_time_topic_name. Standard value is: " << control_compute_time_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("boundary_detector_compute_time_topic_name", boundary_detector_compute_time_topic_name_, "/boundary_detector_compute_time")) {
    ROS_WARN_STREAM("Did not load boundary_detector_compute_time_topic_name. Standard value is: " << boundary_detector_compute_time_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("line_detector_compute_time_topic_name", line_detector_compute_time_topic_name_, "/line_detector_compute_time")) {
    ROS_WARN_STREAM("Did not load line_detector_compute_time_topic_name. Standard value is: " << line_detector_compute_time_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("skidpad_detector_compute_time_topic_name", skidpad_detector_compute_time_topic_name_, "/skidpad_detector_compute_time")) {
    ROS_WARN_STREAM("Did not load skidpad_detector_compute_time_topic_name. Standard value is: " << skidpad_detector_compute_time_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("lidar_detection_compute_time_topic_name", lidar_detection_compute_time_topic_name_, "/lidar_detection_compute_time")) {
    ROS_WARN_STREAM("Did not load lidar_detection_compute_time_topic_name. Standard value is: " << lidar_detection_compute_time_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("camera_detection_compute_time_topic_name", camera_detection_compute_time_topic_name_, "/camera_detection_compute_time")) {
    ROS_WARN_STREAM("Did not load camera_detection_compute_time_topic_name. Standard value is: " << camera_detection_compute_time_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("time1_topic_name", time1_topic_name_, "/time1")) {
    ROS_WARN_STREAM("Did not load time1_topic_name. Standard value is: " << time1_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("time2_topic_name", time2_topic_name_, "/time2")) {
    ROS_WARN_STREAM("Did not load time2_topic_name. Standard value is: " << time2_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("time3_topic_name", time3_topic_name_, "/time3")) {
    ROS_WARN_STREAM("Did not load time1_topic_name. Standard value is: " << time3_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("time4_topic_name", time4_topic_name_, "/time4")) {
    ROS_WARN_STREAM("Did not load time4_topic_name. Standard value is: " << time4_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("time5_topic_name", time5_topic_name_, "/time5")) {
    ROS_WARN_STREAM("Did not load time5_topic_name. Standard value is: " << time5_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("time6_topic_name", time6_topic_name_, "/time6")) {
    ROS_WARN_STREAM("Did not load time6_topic_name. Standard value is: " << time6_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("time7_topic_name", time7_topic_name_, "/time7")) {
    ROS_WARN_STREAM("Did not load time7_topic_name. Standard value is: " << time7_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("time8_topic_name", time8_topic_name_, "/time8")) {
    ROS_WARN_STREAM("Did not load time8_topic_name. Standard value is: " << time8_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("time9_topic_name", time9_topic_name_, "/time9")) {
    ROS_WARN_STREAM("Did not load time9_topic_name. Standard value is: " << time9_topic_name_);
    }
    if (!nodeHandle_.param<std::string>("acceleration_visualization_topic_name", acceleration_visualization_topic_name_, "/decision/visual/acceleration")) {
    ROS_WARN_STREAM("Did not load acceleration_visualization_topic_name. Standard value is: " << acceleration_visualization_topic_name_);
    }
    if (!nodeHandle_.param("target_dis_", target_dis_, 80.0)) {
    ROS_WARN_STREAM("Did not load target_dis_. Standard value is: " << target_dis_);
    }
}

void DecisionAndAsState::subscribeToTopics() {
    ROS_INFO("subscribe to topics");
    cameraSubscriber_ = nodeHandle_.subscribe(camera_topic_name_, 1, &DecisionAndAsState::cameraCallback, this);
    slamStateSubscriber_ = nodeHandle_.subscribe(slam_state_topic_name_, 1, &DecisionAndAsState::slamStateCallback, this);
    lidarSubscriber_ = nodeHandle_.subscribe(lidar_topic_name_, 1, &DecisionAndAsState::lidarCallback, this);
    globalCenterLineSubscriber_ = nodeHandle_.subscribe(global_center_line_topic_name_, 1, &DecisionAndAsState::globalCenterLineCallback, this);
    insSubscriber_ = nodeHandle_.subscribe(ins_topic_name_, 1, &DecisionAndAsState::insCallback, this);
    resAndAmiSubscriber_ = nodeHandle_.subscribe(res_and_ami_topic_name_, 1, &DecisionAndAsState::resAndAmiCallback, this);
    controlCommandSubscriber_ = nodeHandle_.subscribe(control_command_topic_name_, 1, &DecisionAndAsState::controlCommandCallback, this);
    controlComputeTimeSubscriber_ = nodeHandle_.subscribe(control_compute_time_topic_name_, 1, &DecisionAndAsState::controlComputeTimeCallback, this);
    boundaryDetectorComputeTimeSubscriber_ = nodeHandle_.subscribe(boundary_detector_compute_time_topic_name_, 1, &DecisionAndAsState::boundaryDetectorComputeTimeCallback, this);
    lineDetectorComputeTimeSubscriber_ = nodeHandle_.subscribe(line_detector_compute_time_topic_name_, 1, &DecisionAndAsState::lineDetectorComputeTimeCallback, this);
    skidpadDetectorComputeTimeSubscriber_ = nodeHandle_.subscribe(skidpad_detector_compute_time_topic_name_, 1, &DecisionAndAsState::skidpadDetectorComputeTimeCallback, this);
    lidarDetectionComputeTimeSubscriber_ = nodeHandle_.subscribe(lidar_detection_compute_time_topic_name_, 1, &DecisionAndAsState::lidarDetectionComputeTimeCallback, this);
    cameraDetectionComputeTimeSubscricber_ = nodeHandle_.subscribe(camera_detection_compute_time_topic_name_, 1, &DecisionAndAsState::cameraDetectionComputeTimeCallback, this);
    time1Subscriber_ = nodeHandle_.subscribe(time1_topic_name_, 1, &DecisionAndAsState::time1Callback, this);
    time2Subscriber_ = nodeHandle_.subscribe(time2_topic_name_, 1, &DecisionAndAsState::time2Callback, this);
    time3Subscriber_ = nodeHandle_.subscribe(time3_topic_name_, 1, &DecisionAndAsState::time3Callback, this);
    time4Subscriber_ = nodeHandle_.subscribe(time4_topic_name_, 1, &DecisionAndAsState::time4Callback, this);
    time5Subscriber_ = nodeHandle_.subscribe(time5_topic_name_, 1, &DecisionAndAsState::time5Callback, this);
    time6Subscriber_ = nodeHandle_.subscribe(time6_topic_name_, 1, &DecisionAndAsState::time6Callback, this);
    time7Subscriber_ = nodeHandle_.subscribe(time7_topic_name_, 1, &DecisionAndAsState::time7Callback, this);
    time8Subscriber_ = nodeHandle_.subscribe(time8_topic_name_, 1, &DecisionAndAsState::time8Callback, this);
    time9Subscriber_ = nodeHandle_.subscribe(time9_topic_name_, 1, &DecisionAndAsState::time9Callback, this); 
}

void DecisionAndAsState::publishToTopics() {
    ROS_INFO("publish to topics");
    decisionAndAsStatePublisher_ = nodeHandle_.advertise<fsd_common_msgs::AsState>(as_state_topic_name_, 1);
    computeTimePublisher_ = nodeHandle_.advertise<fsd_common_msgs::Time>(compute_time_topic_name_, 1);
    accelerationVisualizationPublisher_ = nodeHandle_.advertise<visualization_msgs::MarkerArray>(acceleration_visualization_topic_name_,1) ;
    bePointPublisher_ = nodeHandle_.advertise<visualization_msgs::MarkerArray>("/decision/visual/bepoint",1);
}

void DecisionAndAsState::run() {
    runAlgorithm();
    sendDecisionAndAsState();
    sendComputeTime();
}

void DecisionAndAsState::sendDecisionAndAsState(){
    decisionAndAsStatePublisher_.publish(AsState_);
}

void DecisionAndAsState::runAlgorithm(){
    runDecision();
    runAsState();
}


void DecisionAndAsState::runDecision(){

    std::cout << "###########################################################" << std::endl;

    // for bug and testing fssim
    //resState_ = 1;
    //mission_ = "trackdrive";

    ROS_INFO("We are racing for mission:%s", mission_.c_str());

    //set initial location
    if (resState_ == 1 && i_start < 5){
        ROS_INFO("Set initial Point by current car state");
        initial_x = state_.car_state.x;
        initial_y = state_.car_state.y;
        initial_theta = state_.car_state.theta;
        i_start++;
        sendBEPointVisualization();

        //计算高速循迹的终点
        end_target_td_x = initial_x + dis_new_initial_to_end_ * cos(initial_theta);
        end_target_td_y = initial_y + dis_new_initial_to_end_ * sin(initial_theta);
    }
    if (initial_x != 0 || initial_y != 0)
        ROS_INFO("Initialize location successed! initial_x: %f, initial_y: %f", initial_x, initial_y);
    else
        ROS_INFO("Initialize location failed!");

    //set the last point of the target path to be the end_target_x & end_target_y
    //TODO: need to do better, to judge where is the end of the reference path
    const int size =  globalCenterLine_.poses.size();
    if (size > 2 && i_end < 5){
        end_target_x = globalCenterLine_.poses[size -1].pose.position.x;
        end_target_y = globalCenterLine_.poses[size -1].pose.position.y;
        i_end++;
        sendBEPointVisualization();
        ROS_INFO("set last point of the target path successed! end_target_x: %f, end_target_y: %f", end_target_x, end_target_y);
    }

    //set the mission, and using different method to make decision
    if (mission_ == "inspection"){
        //build new thread to call the roslaunch, only build once
        if (i1 == 0){
            int ret = pthread_create(&id1, NULL, runInspectionLaunch, this);
            i1++;
        }
        if (((ros::Time::now().toSec() - receive_resState_time_) > 25) && (resAndAmi_.resState == 1)){ //in mission inspection, after receive controlCommand for 25sec, send end signal
            end_ = 1; // 1 means that mission accomplished
            ROS_INFO("mission:%s accomplished!", mission_.c_str());
        }
        else 
            end_ = 0;
    }

    if (mission_ == "ebs_test" && i_end != 0){
        if (i2 == 0){
            int ret = pthread_create(&id2, NULL, runEbsTestLaunch, this);
            i2++;
        }
        //figure out whether it is time to stop
        if (std::hypot(state_.car_state.x - end_target_x, state_.car_state.y - end_target_y) < dis_target_end_){
            end_ = 1; 
            ROS_INFO("mission:%s accomplished!", mission_.c_str());
        }
        else
            end_ = 0;
    }

    if (mission_ == "acceleration" && i_end != 0){
        if (i3 == 0){
            int ret = pthread_create(&id3, NULL, runAccelerationLaunch, this);
            i3++;
        }
        /*
        if (std::hypot(state_.car_state.x - end_target_x, state_.car_state.y - end_target_y) < dis_target_end_){
            end_ = 1; 
            ROS_INFO("mission:%s accomplished!", mission_.c_str());
        }
        */
       
       current_dis_ = std::hypot((state_.car_state.x - last_state_.car_state.x) , (state_.car_state.y - last_state_.car_state.y));
       if(current_dis_ > target_dis_){
        end_ = 1;
        ROS_INFO("mission:%s accomplished!", mission_.c_str());
       }
        else{
            ROS_INFO("Current dis is %f ",current_dis_);
            ROS_INFO("There is %f m to the end", (target_dis_ - current_dis_));
            sendAccelerationVisualization();
            end_ = 0;
       }
    }

    if (mission_ == "skidpad" && i_end != 0){
        if (i4 == 0){
            int ret = pthread_create(&id4, NULL, runSkidpadLaunch, this);
            i4++;
        }
        if (std::hypot(state_.car_state.x - end_target_x, state_.car_state.y - end_target_y) < dis_target_end_){
            end_ = 1; 
            ROS_INFO("mission:%s accomplished!", mission_.c_str());
        }
        else
            end_ = 0;
    }

    if (mission_ == "trackdrive"){
        if (i5 == 0){
            int ret = pthread_create(&id5, NULL, runTrackDriveLaunch, this);
            i5++;
        }
        //count lap
        //TODO: can be better
        if (std::hypot(initial_x - state_.car_state.x, initial_y - state_.car_state.y) < (dis_initial_to_end_ - 0.2)){
            closeToInitial1 = 1;
        }
        if ((closeToInitial1 == 1) && (std::hypot(initial_x - state_.car_state.x, initial_y - state_.car_state.y) > dis_initial_to_end_)){
            leaveFromInitial1 = 1;
        }
        if ((closeToInitial1 == 1) && (leaveFromInitial1 == 1)){
            whichLap_++;   
            closeToInitial1 = 0;
            leaveFromInitial1 = 0;                    
        }
        //figure out whether it is time to stop
        if (whichLap_ > maxLapCount_){
            end_ = 1; // 1 means that mission accomplished
            ROS_INFO("mission:%s accomplished!", mission_.c_str());
        }
        else
            end_ = 0;
    }

    //when the lap is ended && car speed == 0 && car acceleration == 0, it's time to send finish
    if (finished_ == 0 && end_ == 1 && (std::hypot(state_.car_state_dt.car_state_dt.x, state_.car_state_dt.car_state_dt.y) < 0.01) && (std::sqrt(ins_.Acc_x * ins_.Acc_x + ins_.Acc_y * ins_.Acc_y + ins_.Acc_z * ins_.Acc_z) < 1.01)){
        sleep(1);
        finished_ = 1;
    }

    AsState_.mission = mission_;
    AsState_.whichLap = whichLap_;
    AsState_.end = end_;
    AsState_.finished = finished_;

    std::cout << "AsState_.mission:" << mission_ << "\nAsState_.whichLap:" << whichLap_ << "\nAsState_.end:" << end_ << "    (1 means mission accomplished)"
    << "\nAsState_.finished:" << finished_ << "    (1 means mission accomplished and the car stop)"<<"\nresAndAmi.resState: "<<resState_<<" (1 means received GO signal)" << std::endl;
}

void DecisionAndAsState::runAsState(){
    //1. cameraState_
    (((ros::Time::now().toSec() - receive_camera_time_) > 2.0) ? cameraState_ = 1 : cameraState_ = 0);
    //2. lidarState_
    (((ros::Time::now().toSec() - receive_lidar_time_) > 2.0) ? lidarState_ = 1 : lidarState_ = 0);
    //3. insState_
    //in the mission: inspection, do not request the ins initialize successed
    //in the other mission, only when the ins initialize successed(ins_.Status == 2), the ins was set to be online
    if (mission_ == "inspection")   
        (((ros::Time::now().toSec() - receive_ins_time_) > 2.0) ? insState_ = 1 : insState_ = 0);
    else
        ((((ros::Time::now().toSec() - receive_ins_time_) > 2.0) && (ins_.Status == 2)) ? insState_ = 1 : insState_ = 0);
    //4. set ready_
    ((cameraState_ == 0 && lidarState_ == 0 && insState_ == 0 && controlCommandState_ == 1 && slamStatestate_ == 1) ? ready_ = 0 : ready_ = 1);
    //5. set the sensorState_, which is for sensor detecte(ECU, As State Machine)
    ((cameraState_ == 0 && lidarState_ == 0 && insState_ == 0) ? sensorState_ = 0 : sensorState_ = 1);

    AsState_.cameraState = cameraState_;
    AsState_.lidarState = lidarState_;
    AsState_.insState = insState_;
    AsState_.ready = ready_;
    AsState_.sensorState = sensorState_;
    AsState_.header.stamp = ros::Time::now();

    std::cout << "AsState_.cameraState: " << cameraState_ << "    (1 means offline)" << "\nAsState_.lidarState: " << lidarState_ << "    (1 means offline)"
    << "\nAsState_.insState: " << insState_ << "    (1 means offline)" << "\nAsState_.ready: " << ready_ << "    (0 means AS algorithm and all the sensors are ready)"
    << "\nAsState_.sensorState: " << sensorState_ << "    (1 means one of the sensors offline)" << std::endl;
}

std::string DecisionAndAsState::doubleToString(double num){
    std::stringstream ss;
    std::string str;
    ss<<num;
    ss>>str;
    return str;
}



void DecisionAndAsState::sendComputeTime(){
    //compute the sum compute time
    sum_compute_time_ = 
    control_compute_time_ + boundary_detector_compute_time_ + line_detector_compute_time_ + 
    skidpad_detector_compute_time_ + lidar_detection_compute_time_ + camera_detection_compute_time_ + time1_ +
    time2_ + time3_ + time4_ + time5_ + time6_ + time7_ + time8_ + time9_;

    time_.sum_compute_time = static_cast<float>(sum_compute_time_);
    time_.control_compute_time = static_cast<float>(control_compute_time_);
    time_.boundary_detector_compute_time = static_cast<float>(boundary_detector_compute_time_);
    time_.line_detector_compute_time = static_cast<float>(line_detector_compute_time_);
    time_.skidpad_detector_compute_time = static_cast<float>(skidpad_detector_compute_time_);
    time_.lidar_detection_compute_time =  static_cast<float>(lidar_detection_compute_time_);
    time_.camera_detection_compute_time = static_cast<float>(camera_detection_compute_time_);
    time_.time1 = static_cast<float>(time1_);
    time_.time2 = static_cast<float>(time2_);
    time_.time3 = static_cast<float>(time3_);
    time_.time4 = static_cast<float>(time4_);
    time_.time5 = static_cast<float>(time5_);
    time_.time6 = static_cast<float>(time6_);
    time_.time7 = static_cast<float>(time7_);
    time_.time8 = static_cast<float>(time8_);
    time_.time9 = static_cast<float>(time9_);
    time_.header.stamp = ros::Time::now();

    std::cout << "sum_compute_time: " << sum_compute_time_ << " [ms]" << std::endl;
    std::cout << "control_compute_time: " << control_compute_time_ << " [ms]" << std::endl;
    std::cout << "boundary_detector_compute_time: " << boundary_detector_compute_time_ << " [ms]" << std::endl;
    std::cout << "line_detector_compute_time: " << line_detector_compute_time_ << " [ms]" << std::endl;
    std::cout << "skidpad_detector_compute_time: " << skidpad_detector_compute_time_ << " [ms]" << std::endl;
    std::cout << "lidar_detection_compute_time: " << lidar_detection_compute_time_ << " [ms]" << std::endl;
    std::cout << "camera_detection_compute_time: " << camera_detection_compute_time_ << " [ms]" << std::endl;
    std::cout << "time1: " << time1_ << " [ms]" << std::endl;
    std::cout << "time2: " << time2_ << " [ms]" << std::endl;
    std::cout << "time3: " << time3_ << " [ms]" << std::endl;
    //std::cout << "time4: " << time4_ << " [ms]" << std::endl;
    //std::cout << "time5: " << time5_ << " [ms]" << std::endl;
    //std::cout << "time6: " << time6_ << " [ms]" << std::endl;
    //std::cout << "time7: " << time7_ << " [ms]" << std::endl;
    //std::cout << "time8: " << time8_ << " [ms]" << std::endl;
    //std::cout << "time9: " << time9_ << " [ms]" << std::endl;

    computeTimePublisher_.publish(time_);
}

void DecisionAndAsState::slamStateCallback(const fsd_common_msgs::CarState &state){
    state_ = state;
    slamStatestate_ = 1; //1 means that there are slamState publishing
}

void DecisionAndAsState::cameraCallback(const sensor_msgs::Image &camera){
    receive_camera_time_ = ros::Time::now().toSec();
}

void DecisionAndAsState::lidarCallback(const sensor_msgs::PointCloud2 &lidar){
    receive_lidar_time_ = ros::Time::now().toSec();
}

void DecisionAndAsState::insCallback(const fsd_common_msgs::AsensingMessage &ins){
    receive_ins_time_ = ros::Time::now().toSec();
    ins_ = ins;
}

void DecisionAndAsState::globalCenterLineCallback(const nav_msgs::Path &globalCenterLine){
    globalCenterLine_ = globalCenterLine;
}

void DecisionAndAsState::resAndAmiCallback(const fsd_common_msgs::ResAndAmi &resAndAmi){
    resAndAmi_ = resAndAmi;
    resState_ = resAndAmi_.resState;
    mission_ = resAndAmi_.mission;
    if (resState_ == 1 && i_resState == 0){
        receive_resState_time_ = ros::Time::now().toSec();
        i_resState++;
    }
}

void DecisionAndAsState::controlCommandCallback(const fsd_common_msgs::ControlCommand &ControlCommand){
    receive_ControlCommand_time_ = ros::Time::now().toSec();
    controlCommandState_ = 1; //1 means that there are controlCommand publishing
}

void DecisionAndAsState::controlComputeTimeCallback(const std_msgs::Float32 &controlComputeTime){
    control_compute_time_ = controlComputeTime.data;
}

void DecisionAndAsState::boundaryDetectorComputeTimeCallback(const std_msgs::Float32 &boundaryDetectorComputeTime){
    boundary_detector_compute_time_ = boundaryDetectorComputeTime.data;
}

void DecisionAndAsState::lineDetectorComputeTimeCallback(const std_msgs::Float32 &lineDetectorComputeTime){
    line_detector_compute_time_ = lineDetectorComputeTime.data;
}

void DecisionAndAsState::skidpadDetectorComputeTimeCallback(const std_msgs::Float32 &skidpadDetectorComputeTime){
    skidpad_detector_compute_time_ = skidpadDetectorComputeTime.data;
}

void DecisionAndAsState::lidarDetectionComputeTimeCallback(const std_msgs::Float32 &lidarDetectionComputeTime){
    lidar_detection_compute_time_ = lidarDetectionComputeTime.data;
}

void DecisionAndAsState::cameraDetectionComputeTimeCallback(const std_msgs::Float32 &cameraDetectionComputeTime){
    camera_detection_compute_time_ = cameraDetectionComputeTime.data;
}

void DecisionAndAsState::time1Callback(const std_msgs::Float32 &time1){
    time1_ = time1.data;
}

void DecisionAndAsState::time2Callback(const std_msgs::Float32 &time2){
    time2_ = time2.data;
}

void DecisionAndAsState::time3Callback(const std_msgs::Float32 &time3){
    time3_ = time3.data;
}

void DecisionAndAsState::time4Callback(const std_msgs::Float32 &time4){
    time4_ = time4.data;
}

void DecisionAndAsState::time5Callback(const std_msgs::Float32 &time5){
    time5_ = time5.data;
}

void DecisionAndAsState::time6Callback(const std_msgs::Float32 &time6){
    time6_ = time6.data;
}

void DecisionAndAsState::time7Callback(const std_msgs::Float32 &time7){
    time7_ = time7.data;
}

void DecisionAndAsState::time8Callback(const std_msgs::Float32 &time8){
    time8_ = time8.data;
}

void DecisionAndAsState::time9Callback(const std_msgs::Float32 &time9){
    time9_ = time9.data;
}

void* DecisionAndAsState::runInspectionLaunch(void*args){
    //FIXME: need to be unmasked, when in the race
    //system("cd /home/mdc/SCUTAracing_driverless_2022/src/8_decision_and_asstate/decision_and_asstate/shell && bash inspection.sh");
    system("cd /home/vonct/scutaracing_driverless_22_5_27/src/8_decision_and_asstate/decision_and_asstate/shell && bash inspection.sh");
    std::cout << "Success run the inspection launch!" << std::endl;
}

void* DecisionAndAsState::runEbsTestLaunch(void*args){
    //system("cd /home/mdc/SCUTAracing_driverless_2022/src/8_decision_and_asstate/decision_and_asstate/shell && bash ebs_test.sh");
    std::cout << "Success run the ebs_test launch!" << std::endl;
}

void* DecisionAndAsState::runAccelerationLaunch(void*args){
    //system("cd /home/mdc/SCUTAracing_driverless_2022/src/8_decision_and_asstate/decision_and_asstate/shell && bash acceleration.sh");
    std::cout << "Success run the acceleration launch!" << std::endl;
}

void* DecisionAndAsState::runSkidpadLaunch(void*args){
    //system("cd /home/mdc/SCUTAracing_driverless_2022/src/8_decision_and_asstate/decision_and_asstate/shell && bash skidpad.sh");
    std::cout << "Success run the skidpad launch!" << std::endl;
}

void* DecisionAndAsState::runTrackDriveLaunch(void*args){
    //system("cd /home/mdc/SCUTAracing_driverless_2022/src/8_decision_and_asstate/decision_and_asstate/shell && bash trackdrive.sh");
    std::cout << "Success run the trackdrive launch!" << std::endl;
}

void DecisionAndAsState::sendBEPointVisualization(){
    if(bePointPublisher_.getNumSubscribers() > 0){
        visualization_msgs::Marker marker;
        visualization_msgs::MarkerArray marker_array;
        std::string frame_id = state_.header.frame_id;
        marker.header.stamp = ros::Time::now();
        marker.header.frame_id = frame_id;
        marker.ns = "decision/bepoint";
        marker.type = visualization_msgs::Marker::CYLINDER;
        marker.action = visualization_msgs::Marker::ADD;
        //set color
        marker.color.r = 0;
        marker.color.g = 0.8078;
        marker.color.b = 0.8196;
        marker.color.a = 1;
        //means this marker would keep subcribed until the master dies
        //指rviz订阅后，就算不再发表该namespace/id的marker，也可以显示
        marker.lifetime = ros::Duration();
		//means u can only see this marker in specific frame even tf exists
        marker.frame_locked = false;
		//(0,0,0,1) in orientation means do not rotate
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;
        //set size
        marker.scale.z = 0.3;
        marker.scale.x = 0.3;
        marker.scale.y = 0.3;
        //set the same id each time to show only one number every time
        marker.id = 0;
        //set location
        marker.pose.position.x  = initial_x;
        marker.pose.position.y  = initial_y;
        marker.pose.position.z = 0.5;
         marker_array.markers.push_back(marker);
         //add text
         marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
         marker.text = "Begin Point";
         marker.id = 1;
         marker.pose.position.z = 3;
         marker_array.markers.push_back(marker);

         //for trackdrive setting end target visual
        if(mission_ == "trackdrive"){
            marker.type = visualization_msgs::Marker::CYLINDER;
            marker.pose.position.x = end_target_td_x;
            marker.pose.position.y = end_target_td_y;
            marker.pose.position.z = 0.5;
            //255 105 180 pink is end 
            marker.color.r = 1;
            marker.color.g = 0.4118;
            marker.color.b = 0.7059;
            marker.id = 2;
            marker_array.markers.push_back(marker);
            //add text
            marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
            marker.id =3;
            marker.pose.position.z = 3;
            marker.text = "End Point Trackdrive";
            marker_array.markers.push_back(marker);
        }
        //i_end大于4表示 已完成终点设置
        if(i_end > 4){
            marker.type = visualization_msgs::Marker::CYLINDER;
            marker.pose.position.x = end_target_x;
            marker.pose.position.y = end_target_y;
            marker.pose.position.z = 0.5;
            //255 105 180 pink is end 
            marker.color.r = 1;
            marker.color.g = 0.4118;
            marker.color.b = 0.7059;
            marker.id = 2;
            marker_array.markers.push_back(marker);
            //add text
            marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
            marker.id =3;
            marker.pose.position.z = 3;
            marker.text = "End Point";
            marker_array.markers.push_back(marker);
        }
        //publish
        bePointPublisher_.publish(marker_array);

    }
}

void DecisionAndAsState::sendAccelerationVisualization(){
    if(accelerationVisualizationPublisher_.getNumSubscribers() > 0){
        static int max_marker_size_ = 0;
        visualization_msgs::MarkerArray marker_array;
        visualization_msgs::Marker marker;
        //get current state's frame_id
        std::string frame_id = state_.header.frame_id;
        //set the frame_id as well as timestamp
        marker.header.stamp = ros::Time::now();
        marker.header.frame_id = frame_id;
        marker.ns = "decision/acceleration";
        //set the marker type
        marker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
        marker.action = visualization_msgs::Marker::ADD;
        //set the color
        marker.color.r = 1;
        marker.color.g = 0;
        marker.color.b = 0;
        marker.color.a = 1;
        //means this marker would keep subcribed until the master dies
        marker.lifetime = ros::Duration();
		//means u can only see this marker in specific frame even tf exists
        marker.frame_locked = false;
		//(0,0,0,1) in orientation means do not rotate
        marker.pose.orientation.x = 0.0;
        marker.pose.orientation.y = 0.0;
        marker.pose.orientation.z = 0.0;
        marker.pose.orientation.w = 1.0;
        //set size
        marker.scale.z = 0.5;
        marker.scale.x = 0.5;
        marker.scale.y = 0.5;
        //set the same id each time to show only one number every time
        marker.id = 0;
        //set location
        marker.pose.position.x  = state_.car_state.x;
        marker.pose.position.y = state_.car_state.y;
        marker.pose.position.z = 2;
        //set text
        double left_dis = target_dis_ - current_dis_;
        marker.text = doubleToString(left_dis);
        //push it to markerarray
        marker_array.markers.push_back(marker);
        accelerationVisualizationPublisher_.publish(marker_array);
    }
}





}
