
#include "ros/ros.h"
#include <geometry_msgs/PolygonStamped.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>
#include "fsd_common_msgs/AsState.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/CarStateDt.h"
#include "fsd_common_msgs/AsensingMessage.h"
#include "fsd_common_msgs/ControlCommand.h"
#include "fsd_common_msgs/ResAndAmi.h"
#include "fsd_common_msgs/Time.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/String.h"
#include "nav_msgs/Path.h"
#include "visualization_msgs/MarkerArray.h"
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Pose.h>

namespace ns_decision_and_asstate {

class DecisionAndAsState {

 public:

    // Constructor
    explicit DecisionAndAsState(ros::NodeHandle &nodeHandle);

    // Getters
    int getNodeRate() const;

    // Methods
    void loadParameters();
    void subscribeToTopics();
    void publishToTopics();
    void run();
    void runAlgorithm();
    void runDecision();
    void runAsState();
    void sendDecisionAndAsState();
    void sendComputeTime();

    //for rviz
    std::string doubleToString(double num);
    void sendBEPointVisualization();          //接受到GO信号后，发送起点（begin）以及终点（end）marker
    void sendAccelerationVisualization();   //在rviz中显示终点/还有多少米就停
    void sendSkipadVisualization();             
    void sendTrackdriveVisualization();

    //callback
    void cameraCallback(const sensor_msgs::Image &camera);
    void slamStateCallback(const fsd_common_msgs::CarState &state);
    void lidarCallback(const sensor_msgs::PointCloud2 &lidar);
    void globalCenterLineCallback(const nav_msgs::Path &globalCenterLine);
    void insCallback(const fsd_common_msgs::AsensingMessage &ins);
    void resAndAmiCallback(const fsd_common_msgs::ResAndAmi &resAndAmi);  //wait to be edited
    void controlCommandCallback(const fsd_common_msgs::ControlCommand &ControlCommand);

    //callback that used to calculate compute time
    void controlComputeTimeCallback(const std_msgs::Float32 &controlComputeTime);
    void boundaryDetectorComputeTimeCallback(const std_msgs::Float32 &boundaryDetectorComputeTime);
    void lineDetectorComputeTimeCallback(const std_msgs::Float32 &lineDetectorComputeTime);
    void skidpadDetectorComputeTimeCallback(const std_msgs::Float32 &skidpadDetectorComputeTime);
    void lidarDetectionComputeTimeCallback(const std_msgs::Float32 &lidarDetectionComputeTime);
    void cameraDetectionComputeTimeCallback(const std_msgs::Float32 &cameraDetectionComputeTime);
    void time1Callback(const std_msgs::Float32 &time1); //back up, do not used yet
    void time2Callback(const std_msgs::Float32 &time2);
    void time3Callback(const std_msgs::Float32 &time3);
    void time4Callback(const std_msgs::Float32 &time4);
    void time5Callback(const std_msgs::Float32 &time5);
    void time6Callback(const std_msgs::Float32 &time6);
    void time7Callback(const std_msgs::Float32 &time7);
    void time8Callback(const std_msgs::Float32 &time8);
    void time9Callback(const std_msgs::Float32 &time9);

    //build new thread to call the roslaunch
    static void* runInspectionLaunch(void*args);
    static void* runEbsTestLaunch(void*args);
    static void* runAccelerationLaunch(void*args);
    static void* runSkidpadLaunch(void*args);
    static void* runTrackDriveLaunch(void*args);

 private:

    ros::NodeHandle nodeHandle_;

    ros::Publisher decisionAndAsStatePublisher_;
    ros::Publisher computeTimePublisher_;

    //for rviz
    ros::Publisher accelerationVisualizationPublisher_;
    ros::Publisher bePointPublisher_;

    ros::Subscriber cameraSubscriber_;
    ros::Subscriber lidarSubscriber_;
    ros::Subscriber insSubscriber_;
    ros::Subscriber slamStateSubscriber_;
    ros::Subscriber globalCenterLineSubscriber_;
    ros::Subscriber resAndAmiSubscriber_;
    ros::Subscriber controlCommandSubscriber_;
    //subscriber for compute time
    ros::Subscriber controlComputeTimeSubscriber_;
    ros::Subscriber boundaryDetectorComputeTimeSubscriber_;
    ros::Subscriber lineDetectorComputeTimeSubscriber_;
    ros::Subscriber skidpadDetectorComputeTimeSubscriber_;
    ros::Subscriber lidarDetectionComputeTimeSubscriber_;
    ros::Subscriber cameraDetectionComputeTimeSubscricber_;
    ros::Subscriber time1Subscriber_;
    ros::Subscriber time2Subscriber_;
    ros::Subscriber time3Subscriber_;
    ros::Subscriber time4Subscriber_;
    ros::Subscriber time5Subscriber_;
    ros::Subscriber time6Subscriber_;
    ros::Subscriber time7Subscriber_;
    ros::Subscriber time8Subscriber_;
    ros::Subscriber time9Subscriber_;

    std::string lidar_topic_name_;
    std::string slam_state_topic_name_;
    std::string camera_topic_name_;
    std::string ins_topic_name_;
    std::string global_center_line_topic_name_;
    std::string as_state_topic_name_;
    std::string control_command_topic_name_;
    std::string res_and_ami_topic_name_;
    std::string compute_time_topic_name_;
    std::string control_compute_time_topic_name_;
    std::string boundary_detector_compute_time_topic_name_;
    std::string line_detector_compute_time_topic_name_;
    std::string skidpad_detector_compute_time_topic_name_;
    std::string lidar_detection_compute_time_topic_name_;
    std::string camera_detection_compute_time_topic_name_;

    //visual
    std::string acceleration_visualization_topic_name_;

    std::string time1_topic_name_;
    std::string time2_topic_name_;
    std::string time3_topic_name_;
    std::string time4_topic_name_;
    std::string time5_topic_name_;
    std::string time6_topic_name_;
    std::string time7_topic_name_;
    std::string time8_topic_name_;
    std::string time9_topic_name_;

    int ready_ = 0;
    int whichLap_ = 0;
    int end_ = 0; //means end of the mission, time to stop the car
    int finished_ = 0; //when it is end, tha car control try to sopt the car, when the car speed =0, then set finish = 1
    int sensorState_ = 0;
    int cameraState_ = 0;
    int lidarState_ = 0;
    int insState_ = 0;
    int controlCommandState_ = 0; //1 means that there are controlCommand publishing
    int slamStatestate_ = 0; //1 means that there are slamState publishing
    int maxLapCount_ = 3;

    int resState_ = 0; //resAndAmiState, 1 means that we have received "go" signal from RES 
    std::string mission_;//resAndAmiState

    double initial_x;
    double initial_y;
    double initial_theta;
    double end_target_x;
    double end_target_y;
    int i_start = 0;
    int i_end = 0;
    int i_resState = 0;
    int node_rate_ = 0;
    int closeToInitial1 = 0;
    int leaveFromInitial1 = 0;
    int closeToInitial2 = 0;
    int leaveFromInitial2 = 0;
    double dis_initial_to_end_ = 1.0;
    double dis_target_end_ = 1.0;

    //for acceleration to judge whther stop
    double target_dis_ = 0;
    double current_dis_ = 0;

    //for trackdrive
    double end_target_td_x;
    double end_target_td_y;
    double dis_new_initial_to_end_ = 6;   //赛车最前面与起点线的距离为6m

    double receive_camera_time_;
    double receive_lidar_time_;
    double receive_ins_time_;
    double receive_ControlCommand_time_;
    double receive_resState_time_;

    float sum_compute_time_ = 0; //sum all the compute time
    float control_compute_time_ = 0;
    float boundary_detector_compute_time_ = 0;
    float line_detector_compute_time_ = 0;
    float skidpad_detector_compute_time_ = 0;
    float lidar_detection_compute_time_ = 0;
    float camera_detection_compute_time_ = 0;
    float time1_ = 0; //spare data
    float time2_ = 0;
    float time3_ = 0;
    float time4_ = 0;
    float time5_ = 0;
    float time6_ = 0;
    float time7_ = 0;
    float time8_ = 0;
    float time9_ = 0;

    //build new thread to call the roslaunch shell
    pthread_t id1;
    pthread_t id2;
    pthread_t id3;
    pthread_t id4;
    pthread_t id5;
    int i1 = 0;
    int i2 = 0;
    int i3 = 0;
    int i4 = 0;
    int i5 = 0;
    int i6 = 0; //为了存储第一次last_state
    int i7=0;

    fsd_common_msgs::AsState         AsState_;
    fsd_common_msgs::ControlCommand  ControlCommand_;
    nav_msgs::Path    globalCenterLine_;
    fsd_common_msgs::CarState        state_;
    fsd_common_msgs::CarState        last_state_;
    fsd_common_msgs::CarStateDt      velocity_;
    fsd_common_msgs::AsensingMessage ins_;
    fsd_common_msgs::ResAndAmi resAndAmi_;  
    sensor_msgs::PointCloud2 lidar;
    sensor_msgs::Image camera;
    fsd_common_msgs::Time time_;

};
}
